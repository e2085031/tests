const ex = require('./stack.js')


test('Retourne le nombre déléments de la pile',() => {
    const stack = new ex.Stack();
    const count = stack.count()

    expect(count).toEqual(0);

});

test('push une valeur suivi de peek() retourne une valeur',() =>{
    const stack = new ex.Stack();
    stack.push(5)
    expect(stack.peek).toEqual(5);
});


test('apres push, isEmpty retourne false',() => {
    const stack = new ex.Stack();
    stack.push(5)
    expect(stack.isEmpty()).toBeFalsy() 
    
});


test('count apres push retourne 1 de plus que le count intial',() =>{
    const stack = new ex.Stack();
    stack.push(5)
    const count1 = stack.count()
    stack.push(5)
    const count2 = stack.count()
    expect(count2).toBeGreaterThan(count1)
});

test('pop diminue count de 1',() =>{
    const stack = new ex.Stack();
    stack.push(5);
    stack.push(2);
    const count1 = stack.count();
    stack.pop();
    const count2 = stack.count();
    expect(count2).toBeLessThan(count1);
});

test('peek ne change pas le count',() =>{
    const stack = new ex.Stack();
    stack.push(5);
    stack.push('allo');
    const count = stack.count();
    stack.peek();
    const count1 = stack.count();

    expect(count1).toEqual(count);
});

test('pop un stack vide retourne null',() =>{
    const stack = new ex.Stack();
    const pop1 = stack.pop()
    expect(pop1).toBeNull()
});

test('peek une pile retourne null',() =>{
    const stack = new ex.Stack();
    const peek1 = stack.peek();
    expect(peek1).toBeNull()
});


test('pop retourne le dernier element empile',()=>{
    const stack = new ex.Stack();
    stack.push(5);
    stack.push(6);
    const pop1 = stack.pop();
    expect(pop1).toEqual(6)
});

test('peek retourne le dernier element empile',()=>{
    const stack = new ex.Stack();
    stack.push(5);
    stack.push(6);
    const peek = stack.peek();
    expect(peek).toEqual(6)
});

test('pop depile 3 element dans l')